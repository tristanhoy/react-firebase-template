const functions = require('firebase-functions')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')()
const cors = require('cors')({ origin: true })
const app = express()

const { authenticateUser } = require('./middleware/authenticate-user')

app.use(cors)
app.use(cookieParser)
app.use(bodyParser.json())
app.use('/authenticated', authenticateUser)

app.get('/ping', (req, res) => res.sendStatus(200))

app.get('/authenticated/hello', (req, res) => {
  res.json({ msg: `Hello ${req.user.name}` })
})

exports.app = functions.https.onRequest(app)
