import React, { useContext } from 'react'
import { store } from './store.js'
import { signin } from './firebase.js'

const Login = () => {
  const globalState = useContext(store)
  const { dispatch } = globalState

  async function signInWithGoogle () {
    dispatch({ type: 'beginLogin' })
    try {
      const user = await signin()
      dispatch({ type: 'completeLogin', user })
    } catch (error) {
      dispatch({ type: 'failLogin', error })
    }
  }

  return (
    <>
      <div>
        <h1>sign in with google</h1>
        <button onClick={signInWithGoogle}>sign in with google</button>
      </div>
    </>
  )
}

export default Login
