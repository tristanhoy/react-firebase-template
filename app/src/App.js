// based on https://github.com/armand1m/react-with-firebase-auth/blob/master/example

import React, { useContext } from 'react'
import { store } from './store.js'

import Authenticated from './Authenticated.js'
import Login from './Login.js'

const Loading = () => (
  <div style={{
    position: 'fixed',
    display: 'flex',
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '2.68em',
    background: 'green',
    color: 'white'
  }}
  >
    Loading..
  </div>
)

const App = () => {
  const { state } = useContext(store)

  switch (state.authStatus) {
    case 'pending': return <Loading />
    case 'logged-in': return <Authenticated />
    default: return <Login />
  }
}

export default App
