import firebase from 'firebase/app'
import 'firebase/auth'
import { firebaseConfig } from './firebase.config.js'

const firebaseApp = firebase.initializeApp(firebaseConfig)
const auth = firebaseApp.auth()

export function getToken () {
  return auth.currentUser.getIdToken()
}

export function signin () {
  return new Promise((resolve, reject) => {
    const unsubscribe = auth.onAuthStateChanged((user, error) => {
      if (error) reject(error)
      resolve(user)

      unsubscribe()
    })

    if (auth.currentUser === null) {
      auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    }
  })
}

export function signout () {
  auth.signOut()
  // clear the __session cookie
  document.cookie = '__session='
}
