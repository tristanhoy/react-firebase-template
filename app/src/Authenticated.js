import React, { useContext, useState } from 'react'
import { store } from './store.js'
import { signout } from './firebase.js'
import server from './server.js'

const Authenticated = () => {
  const globalState = useContext(store)
  const { state, dispatch } = globalState

  const [hello, setHello] = useState()

  function signOut () {
    signout(dispatch)
    dispatch({ type: 'logout' })
  }

  server.getHello()
    .then(response => setHello(response.msg))

  return (
    <>
      <div>
        <h1>sign out</h1>
        <button onClick={signOut}>sign out</button>
      </div>

      <h1>{hello}</h1>

      <div>
        <h1>user data</h1>
        <textarea style={{ width: 350, height: 200 }} value={JSON.stringify(state.user, null, 2)} readOnly />
      </div>
    </>
  )
}

export default Authenticated
