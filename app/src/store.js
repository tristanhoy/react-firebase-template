import React, { createContext, useReducer } from 'react'

const initialState = { authStatus: 'logged-out' }
const store = createContext(initialState)
const { Provider } = store

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'beginLogin':
        return {
          ...state,
          authStatus: 'pending'
        }

      case 'completeLogin':
        return {
          ...state,
          user: action.user,
          authStatus: 'logged-in'
        }

      case 'failLogin':
        return {
          ...state,
          loginError: action.error,
          authStatus: 'failed'
        }

      case 'logout':
        return {
          ...state,
          user: undefined,
          authStatus: 'logged-out'
        }

      default:
        throw Error(`Unrecognized action type ${action.type}`)
    }
  }, initialState)

  return <Provider value={{ state, dispatch }}>{children}</Provider>
}

export { store, StateProvider }
