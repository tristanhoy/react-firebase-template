/* global fetch */
import { getToken } from './firebase.js'
import { endpoints } from './firebase.config.js'

const baseUrl = window.location.href.startsWith('http://localhost')
  ? endpoints.local
  : endpoints.dev

class Server {
  constructor () {
    this.baseUrl = baseUrl
  }

  async getHello () {
    const token = await getToken()
    const headers = { Authorization: `Bearer ${token}` }
    const response = await fetch(`${baseUrl}/authenticated/hello`, { headers })

    return response.json()
  }
}

export default new Server()
