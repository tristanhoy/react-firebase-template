// Replace this object with a snippet from the Firebase console
export let firebaseConfig = {
  apiKey: "AIzaSyAH2iu8dsuw40Sql0Tcc9ilbx-dbFUqCnQ",
  authDomain: "react-firebase-template-ac7c3.firebaseapp.com",
  projectId: "react-firebase-template-ac7c3",
  storageBucket: "react-firebase-template-ac7c3.appspot.com",
  messagingSenderId: "764341434237",
  appId: "1:764341434237:web:8e104e5cd481a7da593e14"
}

const { projectId } = firebaseConfig
const region = 'us-central1' // check that this matches when you run start-api

export let endpoints = {
  local: `http://localhost:5000/${projectId}/${region}/app`,
  dev: `https://${region}-${projectId}.cloudfunctions.net/app`
}
