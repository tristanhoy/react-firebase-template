# React Firebase Template

## Setting up Firebase

1. Create a new project
2. Goto Build => Authentication => Get Started => Sign-in method => enable Google
4. Goto Project Overview => Add a "Web" app and call it "app"
  - Set up hosting
  - Don't copy paste scripts. You don't need them.
  - Don't run any commands. Skip through to the end.
5. Run `firebase use`, choose your project and give it the alias "dev"
5. Find the app in settings (gear icon)
  - Find "Firebase SDK snippet"
  - Select "Config"
  - Update `app/src/firebase.config.js` to match
6. Run `npm run start-api`, and make sure you see the same region in the listening URL

You need to modify the following files:
```
.firebaserc

app/src/firebase.endpoints.js
```

## Commands
```
# Setup the project
npm run setup

# Start a local environment
npm run start


# Deploy to firebase
npm run deploy
```
